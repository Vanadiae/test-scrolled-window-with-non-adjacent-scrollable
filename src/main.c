/* main.c
 *
 * Copyright 2021 vanadiae
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gtk-4.0/gtk/gtk.h>

static void
on_activate (GtkApplication *app)
{
	GtkWindow *window;

	/* It's good practice to check your parameters at the beginning of the
	 * function. It helps catch errors early and in development instead of
	 * by your users.
	 */
	g_assert (GTK_IS_APPLICATION (app));

	/* Get the current window or create one if necessary. */
	window = gtk_application_get_active_window (app);
	if (window == NULL)
		window = g_object_new (GTK_TYPE_WINDOW,
		                       "application", app,
		                       "default-width", 600,
		                       "default-height", 600,
		                       NULL);

  GtkCssProvider *styling = gtk_css_provider_new ();
  gtk_css_provider_load_from_data(styling, "textview > text { background-color: blue; } label { background-color: lightgreen; color: black; }", -1);
  gtk_style_context_add_provider_for_display(gdk_display_get_default(), GTK_STYLE_PROVIDER (styling), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  GtkWidget *scrolled_window = gtk_scrolled_window_new();
  GtkWidget *vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 12);
  gtk_scrolled_window_set_child (GTK_SCROLLED_WINDOW (scrolled_window), vbox);

  const char *texts[] = {
    "1", "12", "123", "1234", "12345", "123456"
  };
  for (guint i = 0; i < G_N_ELEMENTS (texts); i++)
    gtk_box_append (GTK_BOX (vbox), g_object_new (GTK_TYPE_TEXT_VIEW,
                                                "wrap-mode", GTK_WRAP_WORD_CHAR,
                                                "buffer", g_object_new (GTK_TYPE_TEXT_BUFFER,
                                                                        "text", texts[i],
                                                                        NULL),
                                                NULL)
                  );
  gtk_box_append (GTK_BOX (vbox), g_object_new (GTK_TYPE_LABEL,
                                                "label", "Those lines above are textviews. This text is a label\na\nb\nc\nd\ne\nf\ng\nh\ni\nj\nk\nl\nm",
                                                "wrap-mode", PANGO_WRAP_WORD_CHAR,
                                                NULL));

  gtk_window_set_child (window, scrolled_window);
	gtk_widget_show (GTK_WIDGET (window));
}

int
main (int   argc,
      char *argv[])
{
	g_autoptr(GtkApplication) app = NULL;
	int ret;

	/*
	 * Create a new GtkApplication. The application manages our main loop,
	 * application windows, integration with the window manager/compositor, and
	 * desktop features such as file opening and single-instance applications.
	 */
	app = gtk_application_new ("org.broken.Scroll", G_APPLICATION_FLAGS_NONE);

	/*
	 * We connect to the activate signal to create a window when the application
	 * has been lauched. Additionally, this signal notifies us when the user
	 * tries to launch a "second instance" of the application. When they try
	 * to do that, we'll just present any existing window.
	 *
	 * Because we can't pass a pointer to any function type, we have to cast
	 * our "on_activate" function to a GCallback.
	 */
	g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

	/*
	 * Run the application. This function will block until the applicaiton
	 * exits. Upon return, we have our exit code to return to the shell. (This
	 * is the code you see when you do `echo $?` after running a command in a
	 * terminal.
	 *
	 * Since GtkApplication inherits from GApplication, we use the parent class
	 * method "run". But we need to cast, which is what the "G_APPLICATION()"
	 * macro does.
	 */
	ret = g_application_run (G_APPLICATION (app), argc, argv);

	return ret;
}
